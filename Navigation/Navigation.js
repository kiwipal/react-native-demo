
import React from 'react'
import { StyleSheet, Image, Platform, Text} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Search from "../Components/Search"
import Favorites from "../Components/Favorites"
import FilmDetail from "../Components/FilmDetail"
import ShareFilm from '../Components/ShareFilm'
import CameraDemo from '../Components/CameraDemo'

const Stack = createStackNavigator();

function SearchStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="search" component={Search} options={{ title: 'Rechercher un film' }} />
      <Stack.Screen name="filmDetail" component={FilmDetail}
        options={({ navigation, route }) => ({
            title: 'Détail du film'
          })}
        />
    </Stack.Navigator>
  );
}

function FavoriteStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="favorites" component={Favorites} options={{ title: 'Films préférés' }} />
      <Stack.Screen name="filmDetail" component={FilmDetail} options={{ title: 'Détail du film' }}  />
    </Stack.Navigator>
  );
}

function CameraDemoStack() {
    return (
      <Stack.Navigator>
        <Stack.Screen name="cameraDemo" component={CameraDemo} options={{ title: 'Camera Démo' }} />
      </Stack.Navigator>
    );
  }

const Tab = createBottomTabNavigator();

const getActiveBackgroundColor = () => {
  let color;
  switch (Platform.OS) {
    case "ios":
      color = '#BBBBFF';
    break;
    case "android":
      color = '#CCCCFF';
    break;
    case "web":
      color = '#BBBBFF';
    break;
  }
  return color;
}

export default function Navigation() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        tabBarOptions={{
          initialRouteName: "Search",
          keyboardHidesTabBar: true,
          showIcon: true,
          showLabel: false,
          activeBackgroundColor: getActiveBackgroundColor(),
          inactiveBackgroundColor: '#FFFFFF',
          safeAreaInsets: {bottom: 0},
          style:{height:60}
        }}
      >
        <Tab.Screen name="search" component={SearchStack} options={{
          title: 'Rechercher un film',
          tabBarIcon: (tabInfo) => (
            <Image source={require('../Images/search.png')}
                style={styles.icon} />
            )
          }}
        />

        <Tab.Screen name="favorites" component={FavoriteStack} options={{
          title: 'Films préférés',
          tabBarIcon: (tabInfo) => (
            <Image source={require('../Images/fav_1.png')}
                style={styles.icon} />
            )
          }}
        />

        <Tab.Screen name="cameraDemo" component={CameraDemoStack} options={{
          title: 'Demo caméra',
          tabBarIcon: (tabInfo) => (
            <Image source={require('../Images/camera.png')}
                style={styles.icon} />
            )
          }}
        />

      </Tab.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  icon: {
    width: 30,
    height: 30
  }
})
