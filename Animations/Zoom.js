import React from 'react'
import { Animated } from 'react-native'
import { useEffect } from 'react'

export default function Zoom (props) {
    
    const animScale = new Animated.Value(1);

    useEffect(() => {

        Animated.sequence([
            Animated.timing(animScale, {
                toValue: 2,
                duration: 300,
                useNativeDriver: true
            }),
            Animated.timing(animScale, {
                toValue: 1,
                duration: 300,
                useNativeDriver: true
            })
        ]).start();

    }, [animScale]);

    return (
        <Animated.View style={{transform:[{scale: animScale}]}}>
            {props.children}
        </Animated.View>
    )

}