import React from 'react'
import { Animated, Easing} from 'react-native'
import { useEffect, useRef} from 'react'

export default function Bounce (props) {

    const topPosition = useRef(new Animated.Value(50)).current;
    const fadeIn = useRef(new Animated.Value(0)).current;

    useEffect(() => {

        Animated.parallel([
            Animated.timing(topPosition, {
                toValue: 0,
                duration: 1000,
                easing: Easing.elastic(2),
                useNativeDriver: true
            }),
            Animated.timing(fadeIn, {
                toValue: 1,
                duration: 500,
                easing: Easing.linear,
                useNativeDriver: true
            })
        ]).start();

    }, [topPosition])

    return (
        <Animated.View style={{transform:[{translateY: topPosition}], opacity:fadeIn}}>
            {props.children}
        </Animated.View>
    )

}