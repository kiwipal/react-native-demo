import { createStore} from 'redux';
import favoriteFilmsReducer from './Reducers/favoriteFilmsReducer'
import searchReducer from './Reducers/searchReducer'
import { persistCombineReducers } from 'redux-persist'
import AsyncStorage from '@react-native-async-storage/async-storage';

const rootPersistConfig = {
    key: 'root',
    storage: AsyncStorage
}

export default createStore(
    persistCombineReducers(
        rootPersistConfig,
        {
            favoriteFilmsReducer,
            searchReducer
        }
    )
)
