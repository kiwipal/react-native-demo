const initialState = { favoritesFilm: [] }

function favoriteFilmsReducer(state = initialState, action) {

    let nextState;
    
    switch (action.type) {
        case 'TOGGLE_FAVORITE':
            
            const favoriteFilmIndex = state.favoritesFilm.findIndex(item => item.id === action.value.id)

            if ( ~favoriteFilmIndex ) {
                nextState = {
                    ...state,
                    favoritesFilm: state.favoritesFilm.filter( item => item.id !== action.value.id)
                }
            }else{
                nextState = {
                    ...state,
                    favoritesFilm: [
                        ...state.favoritesFilm,
                        action.value
                    ]
                }
            }
            return nextState || state

        break
        default:
            return state
    }
}

export default favoriteFilmsReducer