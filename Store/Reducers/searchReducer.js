const initialState = { search: {count:0} }

function searchReducer(state = initialState, action) {

    let nextState;
    
    switch (action.type) {
        case 'INCREMENT_COUNT_SEARCH':

            

            const count = state.count

            nextState = {
                ...state,
                search: {
                    count: state.search.count + action.value
                }
                
            }

            return nextState || state

        break
        default:
            return state
    }
}

export default searchReducer