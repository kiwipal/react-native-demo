import React from 'react'
import {StyleSheet, TouchableOpacity, Image, Platform, Share} from "react-native"

export default function ShareFilm(props) {

    const {title, message} = props;

    function shareFilm() {
        Share.share({
            title: title,
            message: message
        });
    }

    return (
        <TouchableOpacity style={styles.share} onPress={shareFilm}>
            <Image style={styles.share_image} source={require('../Images/share.png')} /> 
        </TouchableOpacity>
    )

}

const styles = StyleSheet.create({
    ...Platform.select({
        ios: {
            share: {
                position: 'absolute',
                right: 10,
                top: 9
            },
            share_image: {
                width: 22,
                height: 22
            }
        },
        android: {
            share: {
                position: 'absolute',
                width: 60,
                height: 60,
                right: 30,
                bottom: 30,
                borderRadius: 30,
                backgroundColor: '#e91e63',
                justifyContent: 'center',
                alignItems: 'center'
            },
            share_image: {
                width: 30,
                height: 30
            }
        }
    })
});