import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { Camera } from 'expo-camera';

export default function CameraDemo() {

    const [hasPermission, setHasPermission] = useState(null);
    const [type, setType] = useState(Camera.Constants.Type.back);
  
    useEffect(() => {
      (async () => {
        const { status } = await Camera.requestPermissionsAsync();
        setHasPermission(status === 'granted');
      })();
    }, []);
  
    if (hasPermission === null) {
      return <View />;
    }
    if (hasPermission === false) {
      return <Text>No access to camera</Text>;
    }
    return (
      <View style={styles.main_container}>
        <Camera style={styles.camera} type={type}>
          <View>
            <TouchableOpacity
              style={styles.button}
              onPress={() => {
                setType(
                  type === Camera.Constants.Type.back
                    ? Camera.Constants.Type.front
                    : Camera.Constants.Type.back
                );
              }}>
              <Text> Flip </Text>
            </TouchableOpacity>
          </View>
        </Camera>
      </View>
    );

}

const styles = StyleSheet.create({
    main_container: {
        flex: 1,
        backgroundColor:"#000"
    },
    camera: {
      flex: 1
    },
    button: {
        padding: 10,
        fontWeight: 'bold',
        fontSize: 16,
        paddingLeft: 5,
        textAlign: "center",
        backgroundColor: '#FFF',
        borderRadius: 4,
        marginTop:30,
        marginLeft:50,
        marginRight:50
    }
});