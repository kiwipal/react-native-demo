import React from 'react'
import {StyleSheet, View, Text, Image, ScrollView} from "react-native"
import {getFilmsFromAPI} from '../api/TMDBApi'
import ActivityLoading from './ActivityLoading'
import { useState, useEffect} from 'react'
import FilmList from './FilmList'
import { connect } from 'react-redux'

const mapStateToProps = (state) => {
    return {
      favoritesFilm: state.favoriteFilmsReducer.favoritesFilm
    }
}

export default connect(mapStateToProps)(Favorites)

export function Favorites (props) {

    const {favoritesFilm, navigation} = props;

    return (
      <View style={styles.main_container} >
        <FilmList
          navigation={navigation}
          films={favoritesFilm}
          getNextPage={()=>{}}
        />
      </View>
    );

}

const styles = StyleSheet.create({
  main_container: {
      flex: 1,
      backgroundColor:'#000',
      paddingTop:10
  }
});