import React from 'react'
import {StyleSheet, View, FlatList, Text} from "react-native"
import FilmItem from './FilmItem'
import { connect } from 'react-redux'

const mapStateToProps = (state) => {
    return {
        favoritesFilm: state.favoriteFilmsReducer.favoritesFilm
    }
}

export default connect(mapStateToProps)(FilmList)

export function FilmList(props) {

    const {navigation, films, getNextPage, favoritesFilm} = props;

    const isFavorite = function (id) {
        return ~favoritesFilm.findIndex(item => item.id === id) ? 1 : 0;
    }

    return (
        <View style={styles.flatlist_container}>
            <FlatList
                style={styles.flatlist}
                data={films}
                extraData={props.favoritesFilm}
                keyExtractor={(item) => item.id.toString()}
                renderItem={({item}) => <FilmItem
                                            film={item}
                                            isFavorite={isFavorite(item.id)}
                                            navigation={navigation}>
                                        </FilmItem>}
                onEndReachedThreshold={.5}
                onEndReached={() => getNextPage()}
            />
        </View>
    )

}

const styles = StyleSheet.create({
    flatlist_container: {
        flex: 1,
        height:300, /* Fix FlatList scroll bug */
    },
    flatlist: {
        margin: 10
    }
});