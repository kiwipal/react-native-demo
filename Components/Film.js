import React from 'react'
import {StyleSheet, View, Text, Image, TouchableOpacity} from "react-native"
import {getImageFromApi} from '../api/TMDBApi'
import FilmFavoriteIcon from '../Components/FilmFavoriteIcon'
import Zoom from '../Animations/Zoom'
import moment from 'moment'
import numeral from 'numeral'
import { connect } from 'react-redux'
import { useEffect } from 'react'

// Map the redux store's global state with the component Film's properties
// We could have just written : export default connect(state => state)(Film)
// To map the whole global state (it would works)
// But we don't need all the global state here, but just the favorite films list

const mapStateToProps = (state) => {
    return {
        favoritesFilm: state.favoriteFilmsReducer.favoritesFilm
    }
}

export default connect(mapStateToProps)(Film)

export function Film(props) {

    const {
        id,
        title,
        overview,
        vote_average,
        vote_count,
        budget,
        genres,
        backdrop_path,
        poster_path,
        release_date,
        production_companies,
        favoritesFilm
    } = props;

    const isFavorite = function () {
        return ~favoritesFilm.findIndex(item => item.id === id) ? 1 : 0;
    }

    function toggleFavorite() {

        const action = {
            type: "TOGGLE_FAVORITE",
            value: {
                id,
                title,
                overview,
                vote_average,
                vote_count,
                budget,
                genres,
                poster_path,
                backdrop_path,
                release_date,
                production_companies
            }
        };
        
        props.dispatch(action);

    }

    return (
        <View style={styles.main_container}>
            <Image
                style={styles.backdrop}
                source={{uri: getImageFromApi(backdrop_path)}}
            />
            <Text style={styles.title}>{title}</Text>
                <TouchableOpacity style={styles.favorite_container} onPress={toggleFavorite}>
                    <Zoom>
                        <FilmFavoriteIcon isFavorite={isFavorite()} width="30" height="30" />
                    </Zoom>
                </TouchableOpacity>
            <Text style={styles.overview}>{overview}</Text>
            <Text style={styles.meta}>Sorti le {moment(new Date(release_date)).format('DD/MM/YYYY')}</Text>
            <Text style={styles.meta}>Note : {vote_average} / 10</Text>
            <Text style={styles.meta}>Nombre de votes : {vote_count}</Text>
            <Text style={styles.meta}>Budget : {numeral(budget).format('0,0[.]00 $')}</Text>
            <Text style={styles.meta}>Genres : {genres}</Text>
            <Text style={styles.meta}>Compagnie(s) : {production_companies}</Text>
        </View>
    );

}

const styles = StyleSheet.create({
    main_container: {
        flex: 1,
        backgroundColor:'#FFF',
        padding:10,
        borderRadius:5
    },
    title:{
        fontWeight: 'bold',
        fontSize: 35,
        flex: 1,
        flexWrap: 'wrap',
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
        marginBottom: 10,
        color: '#000000',
        textAlign: 'center'
    },
    backdrop: {
        height: 169,
        margin: 5,
        borderRadius:5
    },
    overview:{
        fontStyle: 'italic',
        color: '#666666',
        margin: 5,
        marginBottom: 15
    },
    meta: {
        marginLeft: 5,
        marginRight: 5,
        marginTop: 5
    },
    favorite_container: {
        alignItems: 'center',
        marginBottom:10
    }
});