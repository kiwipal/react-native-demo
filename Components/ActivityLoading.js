import React from 'react'
import {StyleSheet, View, ActivityIndicator} from "react-native"

export default function ActivityLoading(props) {

    const {isLoading,size} = props;

    if (isLoading) {
        return (
            <View style={styles.loading_container}>
                <ActivityIndicator size={size} style={styles.loading_anim} />
            </View>
        )
    }else{
        return null
    }
}

const styles = StyleSheet.create({
    loading_container: {
        position: 'absolute',
        backgroundColor:'rgba(10,5,30,.8)',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        zIndex:9,
        alignItems: 'center',
        justifyContent: 'center'
    },
    loading_anim: {
        padding:5,
        borderRadius:100
    }
})