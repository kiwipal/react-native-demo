import React from 'react'
import {Image} from "react-native"

export default function FilmFavoriteIcon(props) {

    const {
        isFavorite,
        height,
        width
    } = props;

    if (isFavorite == "undefined") {
        isFavorite = 0;
    }

    const getFavIcon = function () {
        if (isFavorite == 1) {
            return require('../Images/fav_1.png');
        }else{
            return require('../Images/fav_0.png');
        }
    }

    return (
        <Image style={{width:Number(width), height: Number(height)}} source={getFavIcon()} />
    );

}