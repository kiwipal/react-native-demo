import React from 'react'
import {StyleSheet, View, Text, Image, TouchableOpacity} from "react-native"
import {getImageFromApi} from '../api/TMDBApi'
import FilmFavoriteIcon from '../Components/FilmFavoriteIcon'

export default function FilmItem(props) {

    const {film, navigation, isFavorite} = props;

    return (
        <TouchableOpacity onPress={() => navigation.navigate('filmDetail', {id_film: film.id})} >
            <View style={styles.main_container}>
                <View style={styles.poster_container}>
                    { film.poster_path != null
                        ? <Image style={styles.poster_image} source={{uri: getImageFromApi(film.poster_path)}} /> 
                        : <View></View>}
                </View>
                <View style={styles.content_container}>
                    <View style={styles.header_container}>
                        <FilmFavoriteIcon isFavorite={isFavorite} width="25" height="25" />
                        <Text style={styles.title_text} numberOfLines={2}>{film.title}</Text>
                        <View style={styles.vote_container}>
                            <Text style={styles.vote_average_text}>{film.vote_average}</Text>
                        </View>
                    </View>
                    <View style={styles.description_container}>
                        <Text style={styles.overview_text} numberOfLines={5}>{film.overview}</Text>
                    </View>
                    <View style={styles.date_container}>
                        <Text style={styles.release_date_text}>Sorti le {film.release_date}</Text>
                    </View>
                </View>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    main_container: {
        height: 190,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginBottom: 20,
        backgroundColor: '#EEE',
        borderRadius: 8
    },
    content_container: {
        flex: 1,
        flexDirection: 'column',
        marginRight: 5,
        padding: 8
    },
    header_container: {
        flexDirection: 'row',
        flex: 3
    },
    description_container: {
        flex: 5,
    },
    date_container: {
        flex: 1,
    },
    poster_container: {
        backgroundColor: '#EEE',
        borderTopLeftRadius: 8,
        borderBottomLeftRadius: 8,
        width: 120,
    },
    vote_container: {
        height: 23,
        backgroundColor: '#AC0000',
        paddingTop: 3,
        paddingBottom: 3,
        paddingLeft: 6,
        paddingRight: 6,
        borderRadius: 4,
    },
    poster_image: {
        width: 120,
        height: 190,
        borderTopLeftRadius: 8,
        borderBottomLeftRadius: 8
    },
    title_text: {
        fontWeight: 'bold',
        fontSize: 20,
        flex: 1,
        flexWrap: 'wrap',
        paddingRight: 10,
        paddingLeft:5
    },
    vote_average_text: {
        color: '#FFF',
    },
    overview_text: {
        fontStyle: 'italic'
    },
    release_date_text: {
        fontWeight: 'bold',
        textAlign: 'right'
    }
})