import React from 'react'
import {StyleSheet, View, TouchableOpacity, TextInput, FlatList, Text, Keyboard, Button, Platform} from "react-native"
import FilmItem from './FilmItem'
import ActivityLoading from './ActivityLoading'
import FilmList from './FilmList'
import Bounce from '../Animations/Bounce'
import {getFilmsFromAPI} from '../api/TMDBApi'
import { useState, useEffect} from 'react'
import { connect } from 'react-redux'

const mapStateToProps = (state) => {
    return {
      favoritesFilm: state.favoriteFilmsReducer.favoritesFilm,
      search: state.searchReducer.search
    }
}

export default connect(mapStateToProps)(Search)

var page = 1;
var totalPages = 0;
var keyword = "";

export function Search(props) {

    const {favoritesFilm, search, navigation} = props;

    const [films, setFilms] = useState([]);
    const [isLoading, setIsLoading] = useState(false);

    const submitSearch = function () {

        setIsLoading(true);

        if (keyword.length > 0) {
            page = 1;
            setFilms([]);
            getFilms();
        }else{
            setIsLoading(false);
            alert("Veuillez saisir un titre de film!");
        }
    }

    const getFilms = function () {

        getFilmsFromAPI(keyword, page).then(data => {
            totalPages = data.total_pages;
            Keyboard.dismiss();
            setFilms(prevState => [...prevState, ...data.results]);
            countSearch();
        })
        .catch(e => {
            console.log("Failed to fetch films");
        })

    }

    const getNextPage = function () {
        if(page <= totalPages) {
            page++;
            getFilms();
        }
    }

    function countSearch() {

        const action = {
            type: "INCREMENT_COUNT_SEARCH",
            value: 1
        };
        
        props.dispatch(action);

    }

    useEffect(
        () => {
            if (films.length > 0) {
                setIsLoading(false);
            }
        },
        [films]
    );

    return (
        <View style={styles.main_container}>
            <ActivityLoading isLoading={isLoading} size="large"></ActivityLoading>
            <Bounce>
                <TextInput
                    placeholder="Titre du film"
                    style={styles.textinput}
                    onChangeText={(text) => {keyword = text}}
                    onSubmitEditing={submitSearch}
                />
                <TouchableOpacity
                    title="Rechercher"
                    style={styles.button}
                    onPress={submitSearch}
                ><Text style={styles.button_text}>Rechercher</Text></TouchableOpacity>
            </Bounce>
            <Text style={styles.search_count}>Vous avez effectué {search.count} recherche{(search.count != 1 && "s" : "")}</Text>
            <FilmList
                navigation={navigation}
                films={films}
                getNextPage={getNextPage}
            />
        </View>
    )

}

const styles = StyleSheet.create({
    main_container: {
        flex: 1,
        ...Platform.select({
            ios: {
              backgroundColor: '#000',
            },
            android: {
              backgroundColor: '#111',
            },
            web: {
                backgroundColor: '#000',
            }
        })
    },
    textinput: {
        marginTop:20,
        marginLeft: 5,
        marginRight: 5,
        padding: 10,
        fontWeight: 'bold',
        fontSize: 16,
        paddingLeft: 5,
        backgroundColor: '#FFF',
        borderRadius: 4
    },
    button: {
        alignItems: "center",
        padding: 10,
    },
    button_text: {
        fontWeight: 'bold',
        fontSize: 16,
        color: '#FFF'
    },
    search_count: {
        color: '#FFF',
        textAlign: 'center'
    }
});