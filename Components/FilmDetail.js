import React from 'react'
import {StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Platform} from "react-native"
import {getFilmDetailFromApi} from '../api/TMDBApi'
import ActivityLoading from './ActivityLoading'
import { useState, useEffect} from 'react'
import Film from './Film'
import ShareFilm from './ShareFilm'

export default function FilmDetail(props) {

    const navigation = props.navigation;
    const {id_film} = props.route.params;
    const [isLoading, setIsLoading] = useState(true);
    const [film, setFilm] = useState('');

    function getShareButton() {
        if (Platform.OS == "android") {
            return <ShareFilm title={film.title} message={film.overview}></ShareFilm>
        }
    }

    useEffect(
        () => {
            if (film != '') {
                setIsLoading(false);               
                // props.navigation.setOptions({ title: film.original_title });

                props.navigation.setOptions({
                    headerRight: () => (
                        <ShareFilm title={film.title} message={film.overview}></ShareFilm>
                    ),
                });

            }
        },
        [film]
    );

    useEffect(
        () => {
            getFilmDetailFromApi(id_film).then(data => {
                setFilm(data);
            })
            .catch(e => {
                alert("Film details request failed!")
            });
        },
        [] 
    );

    return (
        <ScrollView style={styles.main_container}>
            <ActivityLoading isLoading={isLoading} size="large"></ActivityLoading>
            <View>
                { film == '' || <Film
                    id={film.id}
                    title={film.title}
                    overview={film.overview}
                    vote_average={film.vote_average}
                    vote_count={film.vote_count}
                    budget={film.budget}
                    genres={film.genres.map(item => item.name).join(", ")}
                    poster_path={film.poster_path}
                    backdrop_path={film.backdrop_path}
                    release_date={film.release_date}
                    production_companies={film.production_companies.map(item => item.name).join(", ")}
                ></Film>
                }
                {getShareButton()}
            </View>
        </ScrollView>
    );

}

const styles = StyleSheet.create({
    main_container: {
        flex: 1,
        backgroundColor:'#000',
        padding:10
    }
});