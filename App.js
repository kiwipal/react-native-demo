import React from 'react';
import { StatusBar } from 'expo-status-bar';
import {StyleSheet, View} from "react-native"
import Navigation from './Navigation/Navigation'
import { Provider } from 'react-redux'
import Store from './Store/configureStore'
import { persistStore } from 'redux-persist'
import { PersistGate } from 'redux-persist/es/integration/react'

export default function App() {

  let persistor = persistStore(Store)

  return (
    <View style={styles.main_container}>
      <StatusBar style="auto" />
      <Provider store={Store}>
        <PersistGate persistor={persistor}>
          <Navigation />
        </PersistGate>
      </Provider>
    </View>
  );
}

const styles = StyleSheet.create({
  main_container: {
      flex: 1
  }
})