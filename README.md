CRNA = Create React Native Application


## Install dev environement

Installer `NodeJS`, `Brew` et `npm`.

Install Expo Framework
$ sudo npm install -g expo-cli

Install Watchman
$ brew update
$ brew install watchman

Install Android Studio

Run the following commands.

$ [ -d "$HOME/Library/Android/sdk" ] && ANDROID_SDK=$HOME/Library/Android/sdk || ANDROID_SDK=$HOME/Android/Sdk
echo "export ANDROID_SDK=$ANDROID_SDK" >> ~/`[[ $SHELL == *"zsh" ]] && echo '.zshenv' || echo '.bash_profile'`

$ echo "export PATH=$HOME/Library/Android/sdk/platform-tools:\$PATH" >> ~/`[[ $SHELL == *"zsh" ]] && echo '.zshenv' || echo '.bash_profile'`

$ echo 'export PATH=$PATH:~/Library/Android/sdk/platform-tools/' >> ~/.bash_profile source ~/.bash_profile adb devices

## Create an app

Create and initiate an expo CRNA inside current folder
This example will create the `demoApp` folder and intitate it as an expo CRNA app.
$ expo init demoApp

Start expo
$ yarn start

Or start on specific platform
$ yarn android
$ yarn ios
$ yarn web

Choosing iOS, you may get the error :

    Error running `xcrun simctl list devices --json`: xcrun: error: unable to find utility "simctl", not a developer tool or in PATH

Solution is to define the command line location in XCode, as explained here:
https://stackoverflow.com/questions/29108172/xcrun-unable-to-find-simctl


## iOS Simulator

Use this shortcut to eturn to Expo menu on the device
CTRL + CMD + Z

On real iPhone, shake the phone to get the menu.

## Android emulator

An emulator must be started manually using Android Studio or the connection with the emulator will fail.

## Styling components

[Full list of styles per component](https://github.com/vhpoet/react-native-styling-cheat-sheet)

## FAQ

How to use style vars ?